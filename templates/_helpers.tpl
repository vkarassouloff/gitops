{{/*
Expand the name of the chart.
*/}}
{{- define "AppCtx.name" -}}
{{- default .Chart.Name | trunc 63 | trimSuffix "-" }}
{{- end }}


{{/*
Application image tag
We select by default the Chart appVersion or an override in values
*/}}
{{- define "AppCtx.imageTag" }}
{{- $name := default .Chart.AppVersion .Values.image.tag }}
{{- printf "%s" $name }}
{{- end }}


{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "AppCtx.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-"}}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "AppCtx.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "AppCtx.labels" -}}
helm.sh/chart: {{ include "AppCtx.chart" . }}
{{ include "AppCtx.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "AppCtx.selectorLabels" -}}
app.kubernetes.io/name: {{ include "AppCtx.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}



{{- define "AppCtx.front.service.name" -}}
{{- printf "%s-front-service" .Values.name }}
{{- end }}

{{- define "AppCtx.front.service.selectorname" -}}
{{- printf "%s-front" .Values.name }}
{{- end }}


{{- define "AppCtx.front.ingress.name" -}}
{{- printf "%s-front-ingress" .Values.name }}
{{- end }}

{{- define "AppCtx.front.ingress.selectorname" -}}
{{- printf "%s-front" .Values.name }}
{{- end }}

{{- define "AppCtx.front.ingress.secretname" -}}
{{- printf "%s-front" .Values.name }}
{{- end }}


{{- define "AppCtx.front.deployment.name" -}}
{{- printf "%s-front-deployment" .Values.name }}
{{- end }}

{{- define "AppCtx.front.deployment.selectorname" -}}
{{- printf "%s-front" .Values.name }}
{{- end }}

{{- define "AppCtx.front.configmap.name" -}}
{{- printf "%s-front-config" .Values.name }}
{{- end }}

{{- define "AppCtx.postgres.service.selectorname" -}}
{{- printf "%s-formation-vlad" .Values.name }}
{{- end }}

{{- define "AppCtx.postgres.service.name" -}}
{{- printf "%s-pg-service" .Values.name }}
{{- end }}

{{- define "AppCtx.api.service.selectorname" -}}
{{- printf "%s-api" .Values.name }}
{{- end }}

{{- define "AppCtx.api.service.name" -}}
{{- printf "%s-api-service" .Values.name }}
{{- end }}


{{- define "AppCtx.api.service.secretName" -}}
{{- printf "%s-api-tls" .Values.name }}
{{- end }}


{{- define "AppCtx.api.ingress.name" -}}
{{- printf "%s-api-ingress" .Values.name }}
{{- end }}

{{- define "AppCtx.postgres.secret" -}}
{{- printf "%s.%s-formation-vlad.credentials.postgresql.acid.zalan.do" .Values.postgres.dbname .Values.name }}
{{- end }}


{{- define "AppCtx.api.deployment.name" -}}
{{- printf "%s-api-deployment" .Values.name }}
{{- end }}


{{- define "AppCtx.api.deployment.selectorname" -}}
{{- printf "%s-api" .Values.name }}
{{- end }}


{{- define "AppCtx.postgres.formation" -}}
{{- printf "%s-formation" .Values.name }}
{{- end }}

